---
description: Exemple
---

# other

Other text for gitbook

{% embed url="https://gist.github.com/black1277/c275b0d37b7b376b83ff06690927eb51" %}

{% content-ref url="https://app.gitbook.com/o/SDJGAhsshlj8kcAwlqUP/snippet/11268" %}
[Snippet](https://app.gitbook.com/o/SDJGAhsshlj8kcAwlqUP/snippet/11268)
{% endcontent-ref %}

{% content-ref url="https://app.gitbook.com/o/SDJGAhsshlj8kcAwlqUP/snippet/11271" %}
[Other snippet](https://app.gitbook.com/o/SDJGAhsshlj8kcAwlqUP/snippet/11271)
{% endcontent-ref %}
